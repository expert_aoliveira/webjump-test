<?php

require __DIR__.'/vendor/autoload.php';

use App\Core\Core;
use App\Model\ProdutoDao;
use App\Model\CategoriaDao;
use App\Controller\ProdutoController;
use App\Controller\CategoriaController;
use App\Controller\ErroController;

$template = file_get_contents('app/Template/head.php');
echo $template;

$core = new Core;
$body = $core->start($_GET);
echo $body;

$template = file_get_contents('app/Template/footer.php');
echo $template;