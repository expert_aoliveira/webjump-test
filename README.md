
## Tecnologias utilizadas

1. Banco de Dados: MariaDB
2. Templates com TWIG
3. Testes unitários com phpunit (Instalado mas não implementado)

---

## Instruções


1. Clone este repositorio.
2. No terminal, dentro da pasta raiz execute: composer install
3. Abra o phpmyadmin e crie um banco de dados com o nome db_webjump e importe o arquivo localizado na pasta database do projeto, através da opção import do phpmyadmin. 
4. No terminal, dentro da pasta raiz execute o comando: php -S localhost:8080
5. Abra o navegador e acesse: http://localhost:8080
