-- --------------------------------------------------------
-- Servidor:                     127.0.0.1
-- Versão do servidor:           10.1.36-MariaDB - mariadb.org binary distribution
-- OS do Servidor:               Win32
-- HeidiSQL Versão:              11.0.0.5919
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;


-- Copiando estrutura do banco de dados para db_webjump
CREATE DATABASE IF NOT EXISTS `db_webjump` /*!40100 DEFAULT CHARACTER SET utf8 */;
USE `db_webjump`;

-- Copiando estrutura para tabela db_webjump.tb_categoria
CREATE TABLE IF NOT EXISTS `tb_categoria` (
  `codigo` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `nome` varchar(50) CHARACTER SET utf8 NOT NULL,
  PRIMARY KEY (`codigo`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;

-- Copiando dados para a tabela db_webjump.tb_categoria: ~2 rows (aproximadamente)
/*!40000 ALTER TABLE `tb_categoria` DISABLE KEYS */;
INSERT INTO `tb_categoria` (`codigo`, `nome`) VALUES
	(1, 'Calçados'),
	(2, 'Tênis'),
	(5, 'Acessórios');
/*!40000 ALTER TABLE `tb_categoria` ENABLE KEYS */;

-- Copiando estrutura para tabela db_webjump.tb_produto
CREATE TABLE IF NOT EXISTS `tb_produto` (
  `codigo` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `nome` varchar(50) CHARACTER SET utf8 DEFAULT NULL,
  `sku` varchar(50) CHARACTER SET utf8 DEFAULT NULL,
  `preco` float DEFAULT NULL,
  `descricao` longtext CHARACTER SET utf8,
  `quantidade` int(11) DEFAULT NULL,
  `imagem` varchar(250) CHARACTER SET utf8 DEFAULT NULL,
  PRIMARY KEY (`codigo`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=26 DEFAULT CHARSET=latin1;

-- Copiando dados para a tabela db_webjump.tb_produto: ~7 rows (aproximadamente)
/*!40000 ALTER TABLE `tb_produto` DISABLE KEYS */;
INSERT INTO `tb_produto` (`codigo`, `nome`, `sku`, `preco`, `descricao`, `quantidade`, `imagem`) VALUES
	(1, 'Tênis Runner Bolt', '123', 399.99, 'Design confortável semelhante a uma meia\r\nFecho de cadarço\r\nCabedal têxtil adidas Primeknit+\r\nAjuste de contenção Tailored Fibre Placement\r\nTênis de corrida de alto desempenho\r\nPeso: 310 g (tamanho 40)\r\nDrop: 10 mm (calcanhar 22 mm / ponta do pé 12 mm)\r\nSolado Stretchweb com Borracha Continental™\r\nISS National Lab/Laboratory™ é uma marca registrada da NASA e está sendo usada com permissão\r\nCor do artigo: Core Black / Core Black / Boost Blue Violet Met.\r\nCódigo do artigo: EG1341', 100, 'public/images/product/tenis-runner-bolt.png'),
	(2, 'Tênis Nike', '123', 255, 'Lorem Ipsum', 300, 'public\\images\\product\\tenis-2d-shoes0e2e670b308b311d8c23a548919737a1.png');
/*!40000 ALTER TABLE `tb_produto` ENABLE KEYS */;

-- Copiando estrutura para tabela db_webjump.tb_prod_categoria
CREATE TABLE IF NOT EXISTS `tb_prod_categoria` (
  `codigo_produto` int(11) unsigned NOT NULL,
  `codigo_categoria` int(11) unsigned NOT NULL,
  PRIMARY KEY (`codigo_produto`,`codigo_categoria`),
  KEY `FK_tb_prod_categoria_tb_categoria` (`codigo_categoria`),
  CONSTRAINT `FK_tb_prod_categoria_tb_categoria` FOREIGN KEY (`codigo_categoria`) REFERENCES `tb_categoria` (`codigo`),
  CONSTRAINT `FK_tb_prod_categoria_tb_produto` FOREIGN KEY (`codigo_produto`) REFERENCES `tb_produto` (`codigo`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Copiando dados para a tabela db_webjump.tb_prod_categoria: ~2 rows (aproximadamente)
/*!40000 ALTER TABLE `tb_prod_categoria` DISABLE KEYS */;
INSERT INTO `tb_prod_categoria` (`codigo_produto`, `codigo_categoria`) VALUES
	(1, 1),
	(1, 2),
	(2, 1),
	(2, 2);
/*!40000 ALTER TABLE `tb_prod_categoria` ENABLE KEYS */;

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
