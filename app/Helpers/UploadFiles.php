<?php

    namespace App\Helpers;

    class UploadFiles 
    {
        private $image;

        public function upload($file) {

            $image = $file;
            $extensao = strtolower(substr($image['imagem']['name'], -4));

            if ($extensao != '.jpg' && $extensao != '.png') 
            {
                throw new Exception('Extensão do arquivo inválido!');
            } 

            $nome = strtolower(substr($image['imagem']['name'], 0, strripos ($image['imagem']['name'], '.')));
            $novo_nome = $nome.md5(time()).$extensao;
            $diretorio = $_SERVER['DOCUMENT_ROOT'].DIRECTORY_SEPARATOR.'public'.DIRECTORY_SEPARATOR.'images'.DIRECTORY_SEPARATOR.'product'.DIRECTORY_SEPARATOR;
            
            if (!move_uploaded_file($file['imagem']['tmp_name'], $diretorio.$novo_nome)) 
            {
                throw new Exception('Falha ao salvar imagem no servidor'); 
            } 

            return 'public'.DIRECTORY_SEPARATOR.'images'.DIRECTORY_SEPARATOR.'product'.DIRECTORY_SEPARATOR.$novo_nome;
        }
    }