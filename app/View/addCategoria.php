  <!-- Main Content -->
  <main class="content">
    <h1 class="title new-item">{{ action == 'add' ? 'Adicionar Categoria' : 'Editar Categoria' }}</h1>
    
    <form action="{{ action == 'add' ? 'categoria?page=categoria&action=insert' : 'categoria?page=categoria&action=update&codigo=' }}{{action == 'add' ? '' : categoria[0].codigo}}" method="post">
    <input type="hidden" name="codigo" value="{{ categoria[0].codigo is defined ? categoria[0].codigo : '' }}"/>
      <div class="input-field">
        <label for="category-name" class="label">Nome</label>
        <input type="text" id="category-name" name="nome" class="input-text" value="{{ categoria[0].nome is defined ? categoria[0].nome : '' }}"/>
      </div>
      <div class="actions-form">
        <a href="categoria?page=categoria" class="action back">Voltar</a>
        <button class="btn btn-success btn-lg" type="submit" ><i class="fas fa-plus-square"></i> {{ action == 'add' ? ' Cadastrar' : ' Atualizar' }}</button>
      </div>
    </form>
  </main>
  <!-- Main Content -->