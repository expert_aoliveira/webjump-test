  <!-- Main Content -->
  <main class="content">
    <div class="header-list-page">
      <h1 class="title">Produtos</h1>
      <a href="produtos?page=produto&action=add" class="btn btn-success btn-lg"><i class="fas fa-plus-square"></i> Adicionar Produto</a>
    </div>
    <table class="data-grid">
      <tr class="data-row">
        <th class="data-grid-th">
            <span class="data-grid-cell-content">Nome</span>
        </th>
        <th class="data-grid-th">
            <span class="data-grid-cell-content">SKU</span>
        </th>
        <th class="data-grid-th">
            <span class="data-grid-cell-content">Preço</span>
        </th>
        <th class="data-grid-th">
            <span class="data-grid-cell-content">Quantidade</span>
        </th>
        <th class="data-grid-th">
            <span class="data-grid-cell-content">Categoria</span>
        </th>

        <th class="data-grid-th">
            <span class="data-grid-cell-content">Ações</span>
        </th>
      </tr>
      {% for produto in produtos %}
      <tr class="data-row">
        <td class="data-grid-td">
           <span class="data-grid-cell-content">{{ produto.nome }}</span>
        </td>
      
        <td class="data-grid-td">
           <span class="data-grid-cell-content">{{ produto.sku }}</span>
        </td>

        <td class="data-grid-td">
           <span class="data-grid-cell-content">{{ produto.preco }}</span>
        </td>

        <td class="data-grid-td">
           <span class="data-grid-cell-content">{{ produto.quantidade }}</span>
        </td>

        <td class="data-grid-td">
           <span class="data-grid-cell-content">{{ produto.categoria }}</span>
        </td>
      
        <td class="data-grid-td">
          <div class="actions">
            <a href="produtos?page=produto&action=read&codigo={{ produto.codigo }}" class="btn btn-primary"><i class="fas fa-edit"></i></a>
            <a href="produtos?page=produto&action=delete&codigo={{ produto.codigo }}" class="btn btn-danger"><i class="fas fa-trash-alt"></i></a>
          </div>
        </td>
      </tr>
      {% endfor %}
    </table>
  </main>
  <!-- Main Content -->