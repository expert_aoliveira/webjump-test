  <!-- Main Content -->
  <main class="content">
    <div class="header-list-page">
      <h1 class="title">Categorias</h1>
      <a href="categorias?page=categoria&action=add" class="btn-action">Adicionar Nova Categoria</a>
    </div>
    <table class="data-grid">
      <tr class="data-row">
        <th class="data-grid-th">
            <span class="data-grid-cell-content">Código</span>
        </th>
        <th class="data-grid-th">
            <span class="data-grid-cell-content">Nome</span>
        </th>

        <th class="data-grid-th">
            <span class="data-grid-cell-content">Ações</span>
        </th>
      </tr>
      {% for categoria in categorias %}
        <tr class="data-row">
          <td class="data-grid-td">
            <span class="data-grid-cell-content">{{ categoria.codigo }}</span>
          </td>
          <td class="data-grid-td">
            <span class="data-grid-cell-content">{{ categoria.nome }}</span>
          </td>
          <td class="data-grid-td">
            <div class="actions">
            <a href="categorias?page=categoria&action=read&codigo={{ categoria.codigo }}" class="btn btn-primary"><i class="fas fa-edit"></i></a>
            <a href="categorias?page=categoria&action=delete&codigo={{ categoria.codigo }}" class="btn btn-danger"><i class="fas fa-trash-alt"></i></a>
            </div>
          </td>
        </tr>
      {% endfor %}
    </table>
  </main>
  <!-- Main Content -->