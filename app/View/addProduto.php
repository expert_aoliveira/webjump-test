  <!-- Main Content -->
  <main class="content">
    <h1 class="title new-item">{{ action == 'add' ? 'Adicionar Produto' : 'Editar Produto' }}</h1>
    <form action={{ action == 'add' ? 'produto?page=Produto&action=insert' : 'produto?page=Produto&action=update&codigo=' }}{{action == 'add' ? '' : produtos[0].codigo}} method='POST' enctype="multipart/form-data">
    <input type="hidden" id="code" name="codigo" class="input-text" value="{{ produtos[0].codigo is defined ? produtos[0].codigo : '' }}" />
      <div class="input-field">
        <label for="sku" class="label">SKU</label>
        <input type="text" id="sku" name="sku" class="input-text" value="{{ produtos[0].sku is defined ? produtos[0].sku : '' }}" /> 
      </div>
      <div class="input-field">
        <label for="name" class="label">Nome</label>
        <input type="text" id="name" name="nome" class="input-text" value="{{ produtos[0].nome is defined ? produtos[0].nome : '' }}" /> 
      </div>
      <div class="input-field">
        <label for="price" class="label">Preço</label>
        <input type="text" id="price" name="preco" class="input-text" value="{{ produtos[0].preco is defined ? produtos[0].preco : '' }}" /> 
      </div>
      <div class="input-field">
        <label for="quantity" class="label">Quantidade</label>
        <input type="text" id="quantity" name="quantidade" class="input-text" value="{{ produtos[0].quantidade is defined ? produtos[0].quantidade : '' }}" /> 
      </div>
      <div class="input-field">
        <label for="category" class="label">Categorias</label>
        <select multiple id="category" name="categoria[]" class="input-text">
          {% for key,categoria in categorias %}
            <option value="{{ categoria.codigo }}" {{ categoria_produto[key].codigo_categoria is defined ? categoria.codigo == categoria_produto[key].codigo_categoria ? 'selected' : '' }}>{{ categoria.nome }}</option>
          {% endfor %}
        </select>
      </div>
      <div class="input-field">
        <label for="description" class="label">Descrição</label>
        <textarea id="description" name="descricao" class="input-text">{{ produtos[0].descricao is defined ? produtos[0].descricao : '' }}</textarea>
      </div>
      <div class="input-field">
        <label for="description" class="label">Imagem</label>
        <input type="file" id="image" name="imagem" class="input-text" />
      </div>
      <div class="actions-form">
        <a href="produto?page=produto" class="action back">Voltar</a>
        <button class="btn btn-success btn-lg" type="submit" ><i class="fas fa-plus-square"></i> {{ action == 'add' ? ' Cadastrar' : ' Atualizar' }}</button>
      </div>
    </form>
  </main>
  <!-- Main Content -->