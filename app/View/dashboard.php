  <!-- Main Content -->
  <main class="content">
    <div class="header-list-page">
      <h1 class="title">Dashboard</h1>
      <a href="produtos?page=produto&action=add" class="btn btn-success btn-lg"><i class="fas fa-plus-square"></i> Adicionar Novo Produto</a>
    </div>
    <div class="infor">
      Tem(os) {{ produtos|length }} produto(s) cadastrado(s) na loja. 
    </div>
    <ul class="product-list">
      {% for produto in produtos %}
      <li {{ produto.quantidade <= 0 ? 'style="display:none;"' : '' }}>
        <div class="product-image">
          <img src={{ produto.imagem }} layout="responsive" width="164" height="145" alt="Tênis Runner Bolt" />
        </div>
        <div class="product-info">
          <div class="product-name"><span>{{ produto.nome }}</span></div>
          <div class="product-price"><span class="special-price">{{ produto.quantidade }} disponíveis</span> <span>{{ produto.preco }}</span></div>
        </div>
      </li>
      {% endfor %}
    </ul>
  </main>
  <!-- Main Content -->