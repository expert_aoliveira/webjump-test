<?php

    namespace App\Model;

    class Connection
    {
        private static $instance;

        public static function getConnection() 
        {
            if (!isset(self::$instance))
            {
                self::$instance = new \PDO('mysql:host=localhost;dbname=db_webjump;charset=utf8', 'root', '');
                self::$instance->setAttribute(\PDO::ATTR_ERRMODE, \PDO::ERRMODE_EXCEPTION);
                self::$instance->setAttribute(\PDO::ATTR_DEFAULT_FETCH_MODE, \PDO::FETCH_OBJ);
            } 

            return self::$instance;
        }
    }