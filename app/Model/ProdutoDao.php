<?php

namespace App\Model;

use App\Helpers\UploadFiles;

class ProdutoDao 
{
    private $codigo, $nome, $sku, $preco, $descricao, $quantidade, $categoria, $imagem;

    public function create($post, $files)
    {
        
        if (isset($post)) 
        {
            if (!empty($post['nome'])) $nome = $post['nome']; else throw new \Exception('Nome do produto não foi informado.');
            if (!empty($post['sku'])) $sku = $post['sku']; else throw new \Exception('Sku do produto não foi informado.');
            if (!empty($post['preco'])) $preco = $post['preco']; else throw new \Exception('Preço do produto não foi informado.'); 
            if (!empty($post['descricao'])) $descricao = $post['descricao']; else throw new \Exception('Descrição do produto não foi informado.');
            if (!empty($post['quantidade'])) $quantidade = $post['quantidade']; else throw new \Exception('Quantidade do produto não foi informado.');

            if (isset($files)) {
                $upload_files = new UploadFiles;
                $imagem = $upload_files->upload($files);
            } 
            else 
            {
                $imagem = '';
            }
        }
        else 
        {
            throw new \Exception('Falha ao cadastrar.');
        }

        $sql =  'INSERT INTO tb_produto (nome, sku, preco, descricao, quantidade, imagem) VALUES(?,?,?,?,?,?)';

        $conn = Connection::getConnection();
        $stmt = $conn->prepare($sql);
        
        $stmt->bindValue(1, $nome, \PDO::PARAM_STR);
        $stmt->bindValue(2, $sku, \PDO::PARAM_STR);
        $stmt->bindValue(3, $preco, \PDO::PARAM_INT);
        $stmt->bindValue(4, $descricao, \PDO::PARAM_STR);
        $stmt->bindValue(5, $quantidade, \PDO::PARAM_INT);
        $stmt->bindValue(6, $imagem, \PDO::PARAM_STR);
        $lastId = 0;
        try 
        {
            $conn->beginTransaction();
            $stmt->execute();
            $conn->commit();
            $stmt = $conn->query("SELECT LAST_INSERT_ID()");
            $lastId = $stmt->fetchColumn();
        }
        catch (PDOExecption $e) 
        {
            $conn->rollback();
            throw new \Exception('Falha ao registrar produto.');
        }

        if (!empty($post['categoria'])) {
            $categorias = $post['categoria'];
            foreach($categorias as $item) {
                $sql =  'INSERT INTO tb_prod_categoria (codigo_produto, codigo_categoria) VALUES(?,?)';

                $stmt = Connection::getConnection()->prepare($sql);
    
                $stmt->bindValue(1, $lastId, \PDO::PARAM_STR);
                $stmt->bindValue(2, $item, \PDO::PARAM_STR);

                $res = $stmt->execute();

                if ($res == 0) 
                {
                    throw new \Exception("Falha ao inserir categorias do produto.");
                    return false;
                }
            }
        }
        else 
        {
            $categoria = '';
        }

        return true;
    }

    public function readAll()
    {
        $sql =  'SELECT * FROM tb_produto';

        $stmt = Connection::getConnection()->prepare($sql);
        $stmt->execute();

        if ($stmt->rowCount() > 0)
        {
            $result = $stmt->fetchAll(\PDO::FETCH_ASSOC);
            $formatter = new \NumberFormatter('pt_BR',  \NumberFormatter::CURRENCY);
            for ($i=0; $i<count($result); $i++){
                $result[$i]['preco'] = $formatter->formatCurrency($result[$i]['preco'], 'BRL');

                $sql =  'SELECT DISTINCT tc.nome AS `categoria` FROM tb_categoria AS tc INNER JOIN tb_prod_categoria AS tpc ON tpc.codigo_categoria = tc.codigo INNER JOIN tb_produto AS tp ON tp.codigo = tpc.codigo_produto WHERE tpc.codigo_produto = '.$result[$i]['codigo'];

                $stmt = Connection::getConnection()->prepare($sql);
                $stmt->execute();
                $cat = $stmt->fetchAll(\PDO::FETCH_ASSOC);

                for($x=0; $x<count($cat); $x++) 
                {
                    if ( $x < count($cat ) -1 ) {
                        $result[$i]['categoria'] .= $cat[$x]['categoria'] . ' / ';
                    } else {
                        $result[$i]['categoria'] .= $cat[$x]['categoria'];
                    }
                }
            }


            return $result;
        }
        throw new \Exception('Não há produtos cadastrados na loja!') ;
    }

    public function read($get)
    {
        if (isset($get)) 
        {
            if (!empty($get['codigo'])) $codigo = $get['codigo']; else throw new \Exception('Produto não encontrado.');
        }
        else 
        {
            throw new \Exception('Falha ao ler produto.');
        }

        $sql = 'SELECT DISTINCT * FROM tb_produto WHERE codigo = ?';

        $conn = Connection::getConnection();
        $stmt = $conn->prepare($sql);
        $stmt->bindValue(1, $codigo, \PDO::PARAM_INT);
        $stmt->execute();

        if ($stmt->rowCount() > 0)
        {
            $result = $stmt->fetchAll(\PDO::FETCH_ASSOC);
            return $result;
        }
        //throw new \Exception('Registro não encontrado!') ;
    }

    public function update($post)
    {

        if (isset($post)) 
        {
            if (!empty($post['codigo'])) $codigo = $post['codigo']; else throw new \Exception('Produto não encontrado.');
            if (!empty($post['nome'])) $nome = $post['nome']; else throw new \Exception('Nome do produto não foi informado.');
            if (!empty($post['sku'])) $sku = $post['sku']; else throw new \Exception('Sku do produto não foi informado.');
            if (!empty($post['preco'])) $preco = $post['preco']; else throw new \Exception('Preço do produto não foi informado.');
            if (!empty($post['descricao'])) $descricao = $post['descricao']; else throw new \Exception('Descrição do produto não foi informado.');
            if (!empty($post['quantidade'])) $quantidade = $post['quantidade']; else throw new \Exception('Quantidade do produto não foi informado.');
            if (!empty($post['categoria'])) $categoria = $post['categoria']; else throw new \Exception('Categoria do produto não foi informada.');

            if (!empty($post['imagem'])) $imagem = $post['imagem']; else $imagem = '';
        }
        else 
        {
            throw new \Exception('Falha ao atualizar produto.');
        }

        $sql =  'UPDATE tb_produto SET nome = ?, sku = ?, preco = ?, descricao = ?, quantidade = ?, categoria = ?, imagem = ? WHERE codigo = ?';

        $stmt = Connection::getConnection()->prepare($sql);
        $stmt->bindValue(1, $nome, \PDO::PARAM_STR);
        $stmt->bindValue(2, $sku, \PDO::PARAM_STR);
        $stmt->bindValue(3, $preco, \PDO::PARAM_INT);
        $stmt->bindValue(4, $descricao, \PDO::PARAM_STR);
        $stmt->bindValue(5, $quantidade, \PDO::PARAM_INT);
        $stmt->bindValue(6, $codigo, \PDO::PARAM_INT); //Atribui o código do produto no campo categoria
        $stmt->bindValue(7, $imagem, \PDO::PARAM_STR);
        $stmt->bindValue(8, $codigo, \PDO::PARAM_INT);
        $stmt->execute();

        if ($res == 0) 
        {
            throw new \Exception("Falha ao atualizar registro.");
            return false;
        }

        return true;
    }

    public function delete($get)
    {
        if (isset($get)) 
        {
            if (!empty($get['codigo'])) $codigo = $get['codigo']; else throw new \Exception('Produto não encontrado.');
        }
        else 
        {
            throw new \Exception('Falha ao remover produto.');
        }

        $sql = 'DELETE FROM tb_prod_categoria WHERE codigo_produto = ?';

        $stmt = Connection::getConnection()->prepare($sql);
        $stmt->bindValue(1, $codigo);
        $res = $stmt->execute();

        $sql = 'DELETE FROM tb_produto WHERE codigo = ?';

        $stmt = Connection::getConnection()->prepare($sql);
        $stmt->bindValue(1, $codigo);
        $res = $stmt->execute();

        if ($res == 0) 
        {
            throw new \Exception("Falha ao excluir registro.");
            return false;
        }

        return true;
    }
}