<?php

namespace App\Model;

class CategoriaDao 
{
    private $codigo, $nome;

    public function create($post)
    {
        if (isset($post)) 
        {
            if (!empty($post['nome'])) $nome = $post['nome']; else throw new \Exception('Nome da categoria não foi informada.');
        }
        else 
        {
            throw new \Exception('Falha ao cadastrar.');
        }

        $sql =  'INSERT INTO tb_categoria (nome) VALUES (?)';

        $stmt = Connection::getConnection()->prepare($sql);
        $stmt->bindValue(1, $nome, \PDO::PARAM_STR);
        $res = $stmt->execute();

        if ($res == 0) 
        {
            throw new \Exception("Falha ao inserir registro.");
            return false;
        }

        return true;
    }

    public function readAll()
    {
        $sql =  'SELECT * FROM tb_categoria';

        $stmt = Connection::getConnection()->prepare($sql);
        $stmt->execute();

        if ($stmt->rowCount() > 0)
        {
            $result = $stmt->fetchAll(\PDO::FETCH_ASSOC);
            return $result;
        }
        throw new \Exception('Não há categoria cadastrada na loja!') ;
    }

    public function readCategoriasProduto($get)
    {
        if (isset($get)) 
        {
            if (!empty($get['codigo'])) $codigo = $get['codigo']; else throw new \Exception('Produto não encontrado.');
        }
        else 
        {
            throw new \Exception('Falha ao ler produto.');
        }


        $sql =  'SELECT * FROM tb_prod_categoria WHERE codigo_produto = ?';

        $stmt = Connection::getConnection()->prepare($sql);
        $stmt->bindValue(1, $codigo, \PDO::PARAM_INT);
        $stmt->execute();

        if ($stmt->rowCount() > 0)
        {
            $result = $stmt->fetchAll(\PDO::FETCH_ASSOC);
            return $result;
        }
        throw new \Exception('Registro não encontrado!') ;
    }

    public function read($get)
    {
        if (isset($get)) 
        {
            if (!empty($get['codigo'])) $codigo = $get['codigo']; else throw new \Exception('Produto não encontrado.');
        }
        else 
        {
            throw new \Exception('Falha ao ler produto.');
        }


        $sql =  'SELECT * FROM tb_categoria WHERE codigo = ?';

        $stmt = Connection::getConnection()->prepare($sql);
        $stmt->bindValue(1, $codigo, \PDO::PARAM_INT);
        $stmt->execute();

        if ($stmt->rowCount() > 0)
        {
            $result = $stmt->fetchAll(\PDO::FETCH_ASSOC);
            return $result;
        }
        throw new \Exception('Registro não encontrado!') ;
    }

    public function update($post)
    {
        if (isset($post)) 
        {
            if (!empty($post['codigo'])) $codigo = $post['codigo']; else throw new \Exception('Categoria não encontrado.');
            if (!empty($post['nome'])) $nome = $post['nome']; else throw new \Exception('Nome da categoria não foi informada.');
        }
        else 
        {
            throw new \Exception('Falha ao atualizar categoria.');
        }

        $sql =  'UPDATE tb_categoria SET nome = ? WHERE codigo = ?';

        $stmt = Connection::getConnection()->prepare($sql);
        $stmt->bindValue(1, $nome, \PDO::PARAM_STR);
        $stmt->bindValue(2, $codigo, \PDO::PARAM_INT);

        $res = $stmt->execute();

        if ($res == 0) 
        {
            throw new \Exception("Falha ao atualizar registro.");
            return false;
        }

        return true;
    }

    public function delete($get)
    {
        if (isset($get)) 
        {
            if (!empty($get['codigo'])) $codigo = $get['codigo']; else throw new \Exception('Categoria não encontrada.');
        }
        else 
        {
            throw new \Exception('Falha ao remover categoria.');
        }

        $sql =  'DELETE FROM tb_categoria WHERE codigo = ?';

        $stmt = Connection::getConnection()->prepare($sql);
        $stmt->bindValue(1, $codigo, \PDO::PARAM_INT);
        $res = $stmt->execute();

        if ($res == 0) 
        {
            throw new \Exception("Falha ao excluir registro.");
            return false;
        }

        return true;
    }
}