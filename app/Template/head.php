<?php
header('Content-Type: text/html; charset=utf-8');
?>
<!doctype html>
<html lang='pt-br'>
    <head>
        <title>Webjump | Backend Test </title>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width,minimum-scale=1">
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
        <link  rel="stylesheet" type="text/css"  media="all" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.13.0/css/all.min.css" />
        <link  rel="stylesheet" type="text/css"  media="all" href="/public/css/style.css" />
        <link href="https://fonts.googleapis.com/css?family=Open+Sans:400,800" rel="stylesheet">

        <style amp-boilerplate>body{-webkit-animation:-amp-start 8s steps(1,end) 0s 1 normal both;-moz-animation:-amp-start 8s steps(1,end) 0s 1 normal both;-ms-animation:-amp-start 8s steps(1,end) 0s 1 normal both;animation:-amp-start 8s steps(1,end) 0s 1 normal both}@-webkit-keyframes -amp-start{from{visibility:hidden}to{visibility:visible}}@-moz-keyframes -amp-start{from{visibility:hidden}to{visibility:visible}}@-ms-keyframes -amp-start{from{visibility:hidden}to{visibility:visible}}@-o-keyframes -amp-start{from{visibility:hidden}to{visibility:visible}}@keyframes -amp-start{from{visibility:hidden}to{visibility:visible}}</style><noscript><style amp-boilerplate>body{-webkit-animation:none;-moz-animation:none;-ms-animation:none;animation:none}</style></noscript>
    </head>
    <!-- Header -->
    <amp-sidebar id="sidebar" class="sample-sidebar" layout="nodisplay" side="left">
        <div class="close-menu">
            <a on="tap:sidebar.toggle">
                <img src="/public/images/bt-close.png" alt="Close Menu" width="24" height="24" />
            </a>
        </div>
        <a href="/home?page=home"><img src="/public/images/menu-go-jumpers.png" alt="Welcome" width="200" height="43" /></a>
        <div>
            <ul>
                <li><a href="/categorias?page=categoria" class="link-menu">Categorias</a></li>
                <li><a href="/produtos?page=produto" class="link-menu">Produtos</a></li>
            </ul>
        </div>
    </amp-sidebar>
    <header>
        <div class="go-menu">
            <a on="tap:sidebar.toggle">☰</a>
            <a href="/home?page=home" class="link-logo"><img src="/public/images/go-logo.png" alt="Welcome" width="69" height="430" /></a>
        </div>
        <div class="right-box">
            <span class="go-title">Administration Panel</span>
        </div>    
    </header>  
    <!-- Header -->
    <body>