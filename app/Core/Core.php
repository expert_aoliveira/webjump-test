<?php

    namespace App\Core;

    class Core {
        public function start($url) 
        {
            $controller = '\App\Controller\HomeController';
            $acao = 'index';
            
            if(isset($url['page'])) 
            {
                $controller = '\App\Controller\\'.ucfirst($url['page']).'Controller';
            }
            if(isset($url['action'])) 
            {
                $acao = $url['action'];
            }

            if (!class_exists($controller)) {
                $controller = '\App\Controller\ErroController';
            }

            return call_user_func_array(array(new $controller, $acao), []);
        }
    }