<?php

    namespace App\Controller;

    use App\Model\ProdutoDao;

    class HomeController 
    {
        public function index() 
        {
            try {
                $produtos = new ProdutoDao;
                $params['produtos'] = $produtos->readAll();

                $loader = new \Twig\Loader\FilesystemLoader('app/View');
                $twig = new \Twig\Environment($loader);
                $template = $twig->load('dashboard.php');

                $body = $template->render($params);
                //$body = $twig->render('home.php', $produtos);

                return $body;
            } catch (Exception $e) {
                echo $e->getMessage();
            }

            
        }
    }