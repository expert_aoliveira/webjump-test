<?php

    namespace App\Controller;

    use App\Model\CategoriaDao;

    class CategoriaController 
    {
        public function index() 
        {
            try {
                $categorias = new CategoriaDao;
                $params['categorias'] = $categorias->readAll();

                $loader = new \Twig\Loader\FilesystemLoader('app/View');
                $twig = new \Twig\Environment($loader);
                $template = $twig->load('categorias.php');

                $body = $template->render($params);

                return $body;
            } catch (Exception $e) {
                echo "<script>alert('".$e->getMessage()."');</script>";
            }
        }

        public function read() 
        {
            try 
            {
                $categorias = new CategoriaDao;
                $params['categoria'] = $categorias->read($_GET);

                $loader = new \Twig\Loader\FilesystemLoader('app/View');
                $twig = new \Twig\Environment($loader);
                $template = $twig->load('addCategoria.php');

                $body = $template->render($params);

                return $body;
            } 
            catch (Exception $e) 
            {
                echo "<script> alert('".$e->getMessage()."'); </script>";
            }
        }

        public function add() 
        {
            $categorias = new CategoriaDao;
            $params['action'] = $_GET['action'];

            $loader = new \Twig\Loader\FilesystemLoader('app/View');
            $twig = new \Twig\Environment($loader);
            $template = $twig->load('addCategoria.php');
            $body = $template->render($params);
        
            return $body;
        }

        public function insert() 
        {
            try 
            {
                $categorias = new CategoriaDao;
                $categorias->create($_POST);
                echo "<script> alert('Cadastro realizado com sucesso!'); </script>";
                echo "<script> location.href = 'categorias?page=categoria'; </script>";

            } 
            catch (Exception $e) 
            {
                echo "<script> alert('".$e->getMessage()."'); </script>";
            }
        }

        public function delete() 
        {
            try 
            {
                $categorias = new CategoriaDao;
                $categorias->delete($_GET);
                echo "<script> alert('Categoria removida com sucesso!'); </script>";
                echo "<script> location.href = 'categorias?page=categoria'; </script>";
            } 
            catch (Exception $e) 
            {
                echo "<script> alert('".$e->getMessage()."'); </script>";
            }
        }

        public function update() 
        {
            try 
            {
                $categorias = new CategoriaDao;
                $categorias->update($_POST);
                echo "<script> alert('Categoria atualizada com sucesso!'); </script>";
                echo "<script> location.href = 'categorias?page=categoria'; </script>";
            } 
            catch (Exception $e) 
            {
                echo "<script> alert('".$e->getMessage()."'); </script>";
            }
        }
    }