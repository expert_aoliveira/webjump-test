<?php

    namespace App\Controller;

    use App\Model\ProdutoDao;
    use App\Model\CategoriaDao;

    class ProdutoController 
    {
        public function index() 
        {
            try 
            {
                $produtos = new ProdutoDao;
                $params['produtos'] = $produtos->readAll();

                $loader = new \Twig\Loader\FilesystemLoader('app/View');
                $twig = new \Twig\Environment($loader);
                $template = $twig->load('produtos.php');

                $body = $template->render($params);

                return $body;
            } 
            catch (Exception $e) 
            {
                echo "<script> alert('".$e->getMessage()."'); </script>";
            }
        }

        public function read() 
        {
            try 
            {
                $produtos = new ProdutoDao;
                $categorias = new CategoriaDao;
                $params['produtos'] = $produtos->read($_GET);
                $params['categorias'] = $categorias->readAll();
                $params['categoria_produto'] = $categorias->readCategoriasProduto($_GET);

                $loader = new \Twig\Loader\FilesystemLoader('app/View');
                $twig = new \Twig\Environment($loader);
                $template = $twig->load('addProduto.php');

                $body = $template->render($params);

                return $body;
            } 
            catch (Exception $e) 
            {
                echo "<script> alert('".$e->getMessage()."'); </script>";
            }
        }

        public function add() 
        {
            $categorias = new CategoriaDao;
            $params['categorias'] = $categorias->readAll();
            $params['action'] = $_GET['action'];

            $loader = new \Twig\Loader\FilesystemLoader('app/View');
            $twig = new \Twig\Environment($loader);
            $template = $twig->load('addProduto.php');
            $body = $template->render($params);
        
            return $body;
        }

        public function insert() 
        {
            try 
            {
                $produtos = new ProdutoDao;
                $produtos->create($_POST, $_FILES);
                echo "<script> alert('Cadastro realizado com sucesso!'); </script>";
                echo "<script> location.href = 'produtos?page=produto'; </script>";

            } 
            catch (Exception $e) 
            {
                echo "<script> alert('".$e->getMessage()."'); </script>";
            }
        }

        public function delete() 
        {
            try 
            {
                $produtos = new ProdutoDao;
                $produtos->delete($_GET);
                echo "<script> alert('Produto removido com sucesso!'); </script>";
                echo "<script> location.href = 'produtos?page=produto'; </script>";
            } 
            catch (Exception $e) 
            {
                echo "<script> alert('".$e->getMessage()."'); </script>";
            }
        }
    }